# FaalBot.js

This is a personal project using Discord.js and Node.js
  https://discord.js.org/#/
  
The prefix to use a command is '!' and is currently very limited.

Currently the bot doesn't do much at all and is primarily used to automate adding roles in my University's CS&IT Discord (Moved from managing this to just using CarlBot)

## Commands (Prefix: '!')
**!ping** - Mainly for testing purposes. Bot will respond with "pong".

**!cha** - (OWNER) Used to changed the status of the bot (i.e "playing Hi")

**!dagg** - Pulls from the DaggSub folder. My friend Dagg has been getting meme'd on forever, so I stole the idea from PepoBot and to do it with "DaggSubs". This can be modified to be anything.

**!intro** - Used when a person joins the PSU Discord server. They do a small introduction so the mods know who they're talking to. This then adds them to a role which allows them to add classes to themselves.
